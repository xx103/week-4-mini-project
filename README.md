# Week 4 Mini-Project: rust_actix_web_app

This repository contains a simple web application built with Rust using the Actix web framework. It demonstrates a basic web server that serves static files and implements a simple API endpoint to play a chance game.

## Overview

The application provides a web page with a button that users can click to try their luck. Upon clicking the button, a request is sent to the server, which randomly decides whether the user has won a prize based on predefined odds. The result is then displayed to the user.

## rust_actix_web_app
![Function overview](screenshot1.png)
![Function overview](screenshot2.png)
![Function overview](screenshot3.png)


## Docker Image
![Function overview](screenshot4.png)

## Docker Container
![Function overview](screenshot5.png)

## Getting Started

### Prerequisites

- Rust and Cargo (the Rust package manager) installed on your machine.
- Docker (optional, for building and running the application inside a Docker container).

### Running the Application

#### Without Docker

1. **Build the project:**

```shell
cargo build --release
```

2. **Run the application:**

```shell
cargo run --release
```

The server will start, and you can access the web application by navigating to `http://localhost:8080` in your web browser.

#### With Docker

1. **Build the Docker image:**

```shell
docker build -t rust_actix_web_app .
```

2. **Run the Docker container:**

```shell
docker run -p 8080:8080 rust_actix_web_app
```

Again, access the web application at `http://localhost:8080`.

### Project Structure

- `main.rs`: The entry point for the Actix web server.
- `index.html`: The HTML file for the front-end, containing the button and script for the chance game.
- `Cargo.toml` and `Cargo.lock`: Configuration files for Rust dependencies.
- `Dockerfile`: Instructions for building the Docker image.
- `Makefile`: Provides convenient commands for building, running, and cleaning the project.

### Dependencies

- `actix-web`: The web framework for Rust.
- `actix-files`: For serving static files.
- `rand`: For generating random numbers.
