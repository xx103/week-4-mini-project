use actix_files::Files;
use actix_web::{web, App, HttpResponse, HttpServer, Responder};
use rand::Rng;

async fn button_click() -> impl Responder {
    let mut rng = rand::thread_rng();
    let chance: f64 = rng.gen(); // generates a float between 0.0 and 1.0

    let message = if chance < 0.3 {
        "Congratulations! You've won $5!"
    } else if chance < 0.7 {
        "Congratulations! You've won $10!"
    } else {
        "Thank you. Have a nice day."
    };

    HttpResponse::Ok().body(message)
}

#[actix_web::main]
async fn main() -> std::io::Result<()> {
    HttpServer::new(|| {
        App::new()
            .route("/click", web::get().to(button_click))
            .service(Files::new("/", "./static/").index_file("index.html"))
    })
    .bind(("0.0.0.0", 8080))?
    .run()
    .await
}

