.PHONY: build run clean docker-build docker-run all

# Compile the project
build:
	cargo build --release

# Run the project
run: build
	cargo run --release

# Clean the project
clean:
	cargo clean
	docker rmi -f rust_actix_web_app

# Build the Docker image
docker-build:
	docker build -t rust_actix_web_app .

# Run the Docker container
docker-run: docker-build
	docker run -p 8080:8080 rust_actix_web_app

	
# Command to run all steps
all: build docker-build docker-run

