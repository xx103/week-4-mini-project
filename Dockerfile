FROM rust:1-slim-bookworm AS builder

WORKDIR /code

# Create a new Rust project, only to leverage caching for dependencies.
# This step ensures that your dependencies will be cached unless your Cargo.toml changes.
RUN cargo init

# Copy your project's Cargo.toml and Cargo.lock. 
COPY Cargo.toml Cargo.lock ./

# Fetch the dependencies specified in Cargo.toml, again leveraging Docker caching.
RUN cargo fetch

# Copy the source code 
COPY src/ ./src/

# copy static assets 
COPY static/ ./static/

# Build your application.
RUN cargo build --release

FROM bitnami/minideb:bookworm
WORKDIR /app

# Copy the compiled binary from the builder stage to the final image.
COPY --from=builder /code/target/release/actix_web_app ./actix_web_app

# Copy static files and any other assets your application might need.
COPY --from=builder /code/static ./static

# Indicate what port your server runs on.
EXPOSE 8080

# Run your application.
CMD ["./actix_web_app"]





